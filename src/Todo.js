import React, { Component } from 'react'
import http from 'axios'
import _ from 'lodash'
import TodoHeader from './TodoHeader'
import TodoForm from './TodoForm'
import TodoList from './TodoList'

window.apiUrl = 'http://localhost:2500'

class App extends Component {
  constructor(props) {
    super(props)
    this.state = {
      data: [],
      canAccess: true,
      waitMessage: ''
    }
    this.changeData = this.changeData.bind(this)
    this.changeTodo = this.changeTodo.bind(this)
    this.removeTodo = this.removeTodo.bind(this)
    this.updateState = this.updateState.bind(this)
  }

  componentDidMount() {
    this.updateState({
      canAccess: false,
      waitMessage: 'Please wait...',
    });
    http.get(`${window.apiUrl}/get/0`).then(res => {
      this.updateState({
        data: res.data.task,
        canAccess: true,
        waitMessage: '',
      }, () => {
        console.log(this.state);
      })
    })
  }

  changeData(newData) {
    this.setState({
      data: newData
    })
  }

  updateState(e) {
    this.setState(e);
  }

  changeTodo(todo) {
    this.updateState({
      canAccess: false,
      waitMessage: 'Updating....',
    });
    const newArray = _.cloneDeep(this.state.data)
    const todoIndex = _.findIndex(newArray, ['id', todo.id])
    http.post(`${window.apiUrl}/toggle`, {
      id: todo.id,
    })
      .then((res) => {
        return http.get(`${window.apiUrl}/get/${res.data.id}`);
      })
      .then((res) => {
        if (res.data.task[0].status) {
          newArray[todoIndex].status = true;
        } else {
          newArray[todoIndex].status = false;
        }
        this.changeData(newArray);
        console.log(res);
        this.updateState({
          canAccess: true,
          waitMessage: '',
        });
      });
  }

  removeTodo(todo) {
    const id = {
      id: todo.id,
    }
    this.updateState({
      waitMessage: 'Deleting....',
      canAccess: false,
    });
    http.post(`${window.apiUrl}/del`, id)
      .then((res) => {
        if (res.status === 200) {
          const newArray = _.cloneDeep(this.state.data)
          _.remove(newArray, ['id', todo.id])
          this.changeData(newArray)
          this.updateState({
            waitMessage: '',
            canAccess: true,
          })
        }
      })
  }

  render() {
    return (
      <div>
        <TodoHeader
          dataCount={this.state.data.length} />
        <TodoForm
          data={this.state.data}
          changeData={this.changeData}
          canAccess={this.state.canAccess}
          message={this.state.waitMessage}
          updateState={this.updateState} />
        <TodoList
          removeTodo={this.removeTodo}
          changeTodo={this.changeTodo}
          data={this.state.data}
          canAccess={this.state.canAccess}
        />
      </div>
    )
  }
}

export default App
