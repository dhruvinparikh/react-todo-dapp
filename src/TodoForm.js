import React, { Component } from 'react'
import http from 'axios'

class TodoFrom extends Component {
  constructor() {
    super()
    this.state = {
      content: '',
      author: '',
    }
    this.changeInput = this.changeInput.bind(this)
    this.addTodo = this.addTodo.bind(this)
  }

  changeInput(e) {
    const value = e.target.value;
    const input = e.target.name;
    this.setState({
      [input]: value,
    });
  }

  addTodo(e) {
    e.preventDefault()
    this.props.updateState(
      {
        waitMessage: 'Adding...',
        canAccess: false,
      }
    );
    const newTodo = {
      content: e.target.content.value,
      author: e.target.author.value,
    };
    http.post(`${window.apiUrl}/create`, newTodo)
      .then((res) => {
        this.setState({
          content: '',
          author: '',
        })
        console.log(`[log] ${res.data}`)
        this.props.changeData(
          this.props.data.concat(res.data)
        )
        this.props.updateState(
          {
            waitMessage: '',
            canAccess: true,
          }
        );
      })
  }

  render() {
    return (
      <form onSubmit={this.addTodo}>
        <input type="text"
          name="content"
          value={this.state.content}
          onChange={this.changeInput}
          placeholder='content goes here...'
          disabled={!this.props.canAccess}
        /><br /> <br />
        <input type="text"
          name="author"
          value={this.state.author}
          onChange={this.changeInput}
          disabled={!this.props.canAccess}
          placeholder='author goes here...'
        /><br /><br />
        <input type="submit"
          name="submit"
          value="Submit"
          disabled={!this.props.canAccess} />
        <span>&nbsp;&nbsp;{this.props.message}</span>
      </form>
    )
  }
}

export default TodoFrom
